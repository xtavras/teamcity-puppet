# ==Class: apache::config
#
# Copy conf. files
class apache::config {
  file { 'site-config':
    path    => $apache::params::conffile,
    require => Package[$apache::params::apache],
    content => template('apache/site-config.erb'),
  }
  file { '/var/www/index.html':
    content => "<h1> Puppet on ${::operatingsystem} </h1><p>${apache::params::mygreeting}",
    require => Package[$apache::params::apache],
  }
  file { '/var/www/.htaccess':
    content => template('apache/htaccess.erb'),
    require => Package[$apache::params::apache],
  }
  file { "/etc/${apache::params::apache}/htpasswd":
    content => template('apache/htpasswd.erb'),
    require => Package[$apache::params::apache],
  }
}
