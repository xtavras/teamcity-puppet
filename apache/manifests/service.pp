# ==Class: apache::service
#
# This class is for services
class apache::service {
  service { 'apache':
    ensure     => 'running',
    name       => $apache::params::apache,
    hasrestart => true,
    subscribe  => File['site-config']
    }
}
