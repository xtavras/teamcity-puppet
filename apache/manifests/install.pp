# ==Class: apache::install
#
# Class for packages install
class apache::install {
  package { 'apache':
    ensure => 'installed',
    name   => $apache::params::apache,
    }
}
