# ==Class: apache
#
# Class for Apache
class apache {
  include apache::params
  include apache::install
  include apache::config
  include apache::service

Class['apache::params'] -> Class['apache::install'] -> Class['apache::config'] -> Class['apache::service']
}

