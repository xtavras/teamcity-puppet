class apache::params {
  case $::operatingsystem {
    default: {
      fail ("Not supported OS: ${::operatingsystem}")
    }
    Centos: {
      $apache = 'httpd'
      $conffile = '/etc/httpd/conf.d/vhost.conf'
    }
    Ubuntu, Debian: {
      $apache = 'apache2'
      $conffile = '/etc/apache2/sites-enabled/000-default'
    }
  }
  $mygreeting = hiera('greeting')
  $envname = hiera('envname')
  $htpass = hiera('htpass')
}
