# ==Class: teamcity
#
# Variables for teamcity class
class teamcity::params {
  case $::operatingsystem {
    default: {
      fail ("Not supported OS: ${::operatingsystem}")
    }
    Centos: {
      $jdk_pkg = 'java-1.7.0-openjdk'
    }
    Ubuntu, Debian: {
      $jdk_pkg = 'openjdk-7-jdk'
    }
  }
}
