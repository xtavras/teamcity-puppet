# ==Class: teamcity::install
#
# Install packages for teamcity
class teamcity::install {
  package { 'jdk_pkg':
    ensure  => 'installed',
    name    => $teamcity::params::jdk_pkg,
  }
  package { 'tc_pkg':
    ensure => 'installed',
    name   => 'teamcity',
    }
}
