# ==Class: teamcity::config
#
# Configuration files for Teamcity
class teamcity::config {
  user { 'teamcity':
    ensure     => 'present',
    shell      => '/bin/bash',
    home       => '/home/teamcity',
    managehome => false,
    }
  exec { 'apt-get update':
    command     => '/usr/bin/apt-get update',
    refreshonly => true,
  }
  if $::operatingsystem == Ubuntu {
      file { '/etc/apt/sources.list.d/teamcity':
        source => template('teamcity/teamcity-repo.erb'),
        notify => Exec['apt-get update'],
      }
  }
  else {
    yumrepo { 'teamcity':
      baseurl   => 'http://192.168.122.75',
      gpgcheck  => 0,
    }
  }
}

