# ==Class: teamcity
#
# Install and configure teamcity
class teamcity {
  include teamcity::params
  include teamcity::config
  include teamcity::install
Class['teamcity::params'] -> Class['teamcity::config'] -> Class['teamcity::install']
}

